package com.tnf.inouicar.modele;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tnf.inouicar.R;
import com.tnf.inouicar.dialog.DatePickerFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public Spinner types_vehicules;
    public Spinner nb_portes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.addTypesVehiculesItems();
        this.addNbPortesItems();

        Button button = findViewById(R.id.buttonRechercherVehicule);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rechercherVehicule = new Intent(MainActivity.this, ListeVehiculeActivity.class);
                // récupérer les paramètres des recherches

                //rechercherVehicule.putExtr
                startActivity(rechercherVehicule);
            }
        });
    }

    public void addTypesVehiculesItems() {
        this.types_vehicules = (Spinner) findViewById(R.id.types_vehicules);
        List<String> list = new ArrayList<String>();
        list.add("type 1");
        list.add("type 2");
        list.add("type 3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.types_vehicules.setAdapter(dataAdapter);
    }

    public void addNbPortesItems() {
        this.nb_portes = (Spinner) findViewById(R.id.nb_portes);
        List<String> list = new ArrayList<String>();
        list.add("2");
        list.add("4");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.nb_portes.setAdapter(dataAdapter);
    }

    public void addDateDebut(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "hjhjhjh");
    }

    public void addDateFin(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "ddfdfdf");
    }

}